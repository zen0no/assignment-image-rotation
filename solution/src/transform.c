#include "transform.h"

#include "image.h"


bool  rotate(const struct image* src, struct image* out){
    uint32_t width = src->height;
    uint32_t height = src->width;

    if (!image_allocate(width, height, out)){
        return false;
    }
    for (size_t i = 0; i < src->height; i++){
        for (size_t j = 0; j < src->width; j++){
            *(image_get_pixel(out, width - i - 1, j)) = *(image_get_pixel(src, j, i));
        }
    }

    return true;
}
