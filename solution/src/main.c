#include <stdio.h>
#include <string.h>

#include <malloc.h>

#include "bmp.h"
#include "image.h"
#include "image_transform.h"
#include "status.h"


int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    
    if (argc != 3){
        printf("Wrong usage");
        return WRONG_USAGE;
    }

    struct image image;
    struct image result;
    uint8_t status;

    status = bmp_file_read(argv[1], &image);
    if (status != 0){
        return status;
    }


    if (!image_apply_transformation(&image, &result, &rotate)){
        return IMAGE_TRANSFORMATION_ERROR;
    }

    status = bmp_file_write(argv[2], &result);
    if (status != 0){
        return status;
    }

    image_free(&image);
    image_free(&result);



    return SUCCESS;

}
