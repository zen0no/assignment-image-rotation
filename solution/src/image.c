#include "image.h"


#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>


bool image_allocate(uint32_t width, uint32_t height, struct image *image){
    struct pixel *data = malloc(width * height * sizeof(struct pixel));

    if (data == NULL){
        free(data);
        return false;
    }

    *image = (struct image) {.width = width, .height = height, .data = data};

    return true;

}


bool image_free(struct image *image){
    if (image->data == NULL){
        return false;
    }

    free(image->data);
    return true;
}

struct pixel* image_get_pixel(const struct image* image, uint32_t width, uint32_t height){
    return image->data + (image->width * height + width);
}


