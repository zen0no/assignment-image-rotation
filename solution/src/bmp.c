#include <inttypes.h>
#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>

#include "bmp.h"
#include "status.h"


static const uint32_t HEADER_SIGNATURE = 0x4D42;
static const uint32_t PIXEL_SIZE = sizeof(struct pixel);

static const uint32_t HEADER_INFO_SIZE = 40;
static const uint16_t HEADER_BIT_COUNT = 24;
static const uint32_t HEADER_PPM = 2834;
static const uint16_t HEADER_PLANES = 1;
static const uint16_t HEADER_COMPRESSION = 0;
static const uint32_t HEADER_COLORS = 0;



struct __attribute__((packed)) bmp_header{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;

};

static bool valid_signature(struct bmp_header const *header){
    return (header->bfType == HEADER_SIGNATURE);
}

static uint8_t calculate_padding(uint32_t width){
    const uint8_t padding = (width * PIXEL_SIZE) % 4;
    if (padding == 0){
        return padding;
    }
    return 4 - padding;
}


static bool bmp_read_header(FILE* file, struct bmp_header *header){
    return fread(header, sizeof(struct bmp_header), 1, file);
}



static bool bmp_read_bitmap(FILE* file, struct image *image, uint32_t offset){

    uint32_t items_read;

    const uint8_t padding = calculate_padding(image->width);


    if (fseek(file, offset, SEEK_SET))
        {
            return false;
        }

    for (uint32_t i = 0; i < image->height; i++){
        items_read = fread(image->data + i * image->width, PIXEL_SIZE, image->width, file );


        if(items_read != image->width || fseek(file, padding, SEEK_CUR)){
            return false;
        }
    }


    return true;
}

/*
    reads bmp_file from file
*/
static enum image_rotation_status_code bmp_image_read(FILE* in, struct image *image){
    if (fseek(in, 0, SEEK_SET)) return FILE_SEEK_ERROR;
    struct bmp_header header = {0};

    if (!bmp_read_header(in, &header)){
        return BMP_HEADER_READ_ERROR;
    }

    if (!valid_signature(&header)){
        return BMP_WRONG_SIGNATURE;
    }

    if (!image_allocate(header.biWidth, header.biHeight, image)){
        return IMAGE_ALLOCATION_ERROR;
    }

    if (!bmp_read_bitmap(in, image, header.bOffBits)){
        image_free(image);
        return BMP_BITMAP_READ_ERROR;
    }


    return SUCCESS;
}

enum image_rotation_status_code bmp_file_read(const char* filepath, struct image *image){
    FILE* file = fopen(filepath, "rb");
    if (file == NULL){
        return FILE_NOT_OPENED;
    }
    return bmp_image_read(file, image);
}

uint32_t bmp_bitmap_size(struct image const *image){
    const uint8_t padding = calculate_padding(image->width);
    return (image->width * PIXEL_SIZE + padding) * image->height;
}

static struct bmp_header bmp_header_from_image(struct image const *image){
    struct bmp_header header = {0};

    uint32_t RESERVED = 0;


    header.bfType = HEADER_SIGNATURE;
    header.bfileSize = sizeof(struct bmp_header) + bmp_bitmap_size(image);
    header.bfReserved = RESERVED;
    header.bOffBits = sizeof(struct bmp_header);

    header.biSize = HEADER_INFO_SIZE;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biPlanes = HEADER_PLANES;
    header.biBitCount = HEADER_BIT_COUNT;
    header.biCompression = HEADER_COMPRESSION;
    header.biSizeImage = bmp_bitmap_size(image);
    header.biXPelsPerMeter = HEADER_PPM;
    header.biYPelsPerMeter = HEADER_PPM;
    header.biClrImportant = HEADER_COLORS;
    header.biClrUsed = HEADER_COLORS;
    return header;


}

static enum image_rotation_status_code bmp_write_header(FILE* file, const struct bmp_header *header){
    if (!fwrite(header, sizeof(struct bmp_header), 1, file)) return BMP_HEADER_WRITE_ERROR;
    return SUCCESS;
}

static enum image_rotation_status_code bmp_write_bitmap(FILE* file, const struct image *image, uint32_t offset){
    const uint8_t padding = calculate_padding(image->width);

    uint8_t garbage[3] = {0, 0, 0};
    if (fseek(file, offset, SEEK_SET)) return FILE_SEEK_ERROR;
    for (size_t i = 0; i < image->height; i++){
        uint32_t items_written = fwrite(image->data + (image->width * i), PIXEL_SIZE, image->width, file);

        if (items_written != image->width){
            return BMP_BITMAP_WRITE_ERROR;
        }

        uint32_t garbage_written = fwrite(garbage, sizeof(uint8_t), padding, file);
        if (garbage_written != padding) {
            return BMP_BITMAP_WRITE_ERROR;
        }

    }

    return SUCCESS;
}


static enum image_rotation_status_code bmp_image_write(FILE* out, struct image *image){

    enum image_rotation_status_code code;

    if (fseek(out, 0, SEEK_SET)) return FILE_SEEK_ERROR;
    struct bmp_header header = bmp_header_from_image(image);
    code = bmp_write_header(out, &header);
    if (code) {
        return code;
    }

    code = bmp_write_bitmap(out, image, header.bOffBits);

    if (code){
        return code;
    }

    return SUCCESS;
}


enum image_rotation_status_code bmp_file_write(const char* filepath, struct image *image){
    FILE* out = fopen(filepath, "wb");
    if (out == NULL){
        return FILE_NOT_OPENED;
    }

    return bmp_image_write(out, image);
}
