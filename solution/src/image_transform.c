#include "image_transform.h"

bool image_apply_transformation(const struct image* src, struct image* dist, transformation_t transformation){
    if (!transformation(src, dist)){
        return false;
    }
    return true;
}
