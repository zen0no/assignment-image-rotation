#ifndef IMAGE_ROTATION_IMAGE_H
#define IMAGE_ROTATION_IMAGE_H

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>




struct __attribute__((packed)) pixel{
    uint8_t b, g, r;
};

struct image{
    uint32_t height;
    uint32_t width;

    struct pixel* data;
};

bool image_allocate(uint32_t width, uint32_t height, struct image *image);


bool image_free(struct image *image);

struct pixel* image_get_pixel(const struct image* image, uint32_t width, uint32_t height);



#endif
