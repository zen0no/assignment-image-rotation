#ifndef IMAGE_ROTATION_TRANSFORM_H
#define IMAGE_ROTATION_TRANSFORM_H

#include <inttypes.h>
#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>

#include "image.h"

typedef bool (*transformation_t)(const struct image*, struct image*);

bool  rotate(const struct image* src, struct image* out);

#endif
