#ifndef IMAGE_ROTATION_IMAGE_TRANSFORM
#define IMAGE_ROTATION_IMAGE_TRANSFORM

#include <stdbool.h>

#include "image.h"
#include "transform.h"

bool image_apply_transformation(const struct image* src, struct image* dist, transformation_t transformation);


#endif
