#ifndef IMAGE_ROTATION_BMP_H
#define IMAGE_ROTATION_BMP_H

#include "image.h"
#include "status.h"

#include <inttypes.h>


enum image_rotation_status_code bmp_file_read(const char* filepath, struct image* image);

enum image_rotation_status_code bmp_file_write(const char* filepath, struct image* image);

#endif
